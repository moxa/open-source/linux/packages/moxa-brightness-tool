# moxa-brightness-tool
## br-util
Command line brightness control tool.
### Build
```
cd br-util
make
```

## BrightnessControl
Qt-based brightness control tool.
### Build
```
qmake BrightnessControl.pro
make
```

## mtview
Shows a graphical view of a kernel multitouch device.
### Install dependency library
```
apt install dh-autoreconf libmtdev1 libmtdev-dev libevdev-dev libcairo2-dev
```

